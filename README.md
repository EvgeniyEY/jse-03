# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: EVGENIY ERMOLAEV

**E-MAIL**: ermolaev.evgeniy.96@yandex.ru

# SOFTWARE

- JDK 1.8

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```